<?php
require_once('class.logging.php'); // also defines the constants, so we need this first

/** Communication with several upstream services (Gitea, Github, GitLab)
 *  via their resp. APIs
 */
class upstream {

  protected $conf = [];         // parsed ini file data with sections
  protected $log;               // logging class instance
  protected $ua;                // global user_agent for StreamContext (Gitea, Github)

  /** Setting up the class instance
   * @constructor upstream
   * @param          string inifile  config file to use
   * @param optional object logger   logger instance (default: null = use "null-logger" aka NOLOG)
   * @param optional bool   debug    override logger's debug setting (enforce [no]debug for screen)
   */
  function __construct($inifile,$logger=null,$debug=false) {
    $this->conf = parse_ini_file($inifile, TRUE); // parse with sections
    // init logging
    if ( $logger==null) { // no logger instance passed
      $this->log = new logging('',NOLOG,NOLOG); // dummy
    } else {
      $this->log = $logger;
    }
    $this->setUA("Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0 Waterfox/56.2.7");
    if ($debug) $this->log->setloglevel('screen',DEBUG);
  }

  /** Obtain the UserAgent currently configured for API calls
   * @method getUA
   * @return string ua  the UserAgent string
   */
  public function getUA($ua) {
   return $this->ua;
  }

  /** Set the UserAgent for API calls
   * @method setUA
   * @param string ua   the UserAgent string
   */
  public function setUA($ua) {
   $this->ua = $ua;
  }

  /** Get the AUM (AutoUpdateMode) pattern for RegEx tag name matching
   * @method getAum
   * @protected
   * @param array app app details
   * @return string aum
   */
  protected function getAum($app) {
    if ( empty($app['aum']) ) { // must contain either %c or %v (e.g.: 'v%v'), combination ('%c-%v') not yet handled
      $aum = '';
    } else {
      // top.imlk.undo: "v%v(%c)"
      if ( strpos($app['aum'],'%c') !== false && strpos($app['aum'],'%v') !== false )
        $this->log->warn($app['id'].' uses both $v and %c in AUM, this is not yet handled!');
      $aum = str_replace('%c','(\d+)',$app['aum']);   // TODO: adjust this to return both, %c and %v, if both are used – or %c if only that is used
      $aum = str_replace('%v','(.+)',$aum);
      $aum = str_replace('Version ','',$aum);
    }
    return $aum;
  }

###########################################################[ Codeberg/Gitea ]###
  /** Get the URL context needed for Codeberg API queries (stream_context_create)
   * @method codebergStreamContext
   * @protected
   * @return context created via codebergStreamContext
   * @brief used by getCodebergLastRel
   */
  protected function codebergStreamContext() {
    $opts = array('http' =>
      array(
        'method' => 'GET',
        'header' => "Accept-language: en\r\nAccept-Charset: UTF-8\r\nAuthorization: token ".$this->conf['codeberg']['token']."\r\naccept: application/json\r\n",
        'user_agent' => $this->ua,
        'ignore_errors' => TRUE
      )
    );
    return stream_context_create($opts);
  }


// ----------------------------------------------------------------------------
  /** Get details on a specified Codeberg/Gitea repository
   * @method getCodebergRepoInfo
   * @param array spec      [str host, str owner, str repo, str appId]
   * @return object info    empty when server returned neither 200 nor 404, else repo details (see this::repoInfo)
   */
  function getCodebergRepoInfo($spec) {
    $context = $this->codebergStreamContext();
    foreach($spec as $key => $val) ${$key} = $val;

    $this->log->debug($appId.": looking for 'https://$host/api/v1/repos/${owner}/${repo}'");

    $info = new stdClass();
    $repo = json_decode( file_get_contents("https://$host/api/v1/repos/${owner}/${repo}",false,$context) );
    $resp = explode(' ',$http_response_header[0])[1];
    if ( $resp == "200" ) {
      $this->log->debug($appId.": got repo details, parsing now.");
      foreach(['created_at', 'updated_at', 'default_branch', 'description', 'has_issues', 'has_wiki', 'name', 'size', 'website', 'ssh_url', 'html_url', 'archived'] as $val) $info->{$val} = $repo->{$val};
      $info->gone = 0;
    } elseif ( $resp == "404" ) {
      $this->log->debug($appId.": got a 404, repo gone.");
      $info->gone = 1;
    } else {
      $this->log->debug($appId.": got a $resp resonse, giving up.");
    }

    return $info;
  }


// ----------------------------------------------------------------------------
  /** Check Codeberg/Gitea Releases page of an app for latest version
   * @method getCodebergLastRel
   * @param array app app details
   * @return array[str rel, str file] releaseNo and file name of latest release
   * @brief Using GiteaAPI, see: https://docs.gitea.io/en-us/api-usage/ and https://try.gitea.io/api/swagger#/repository/repoListReleases
   */
  function getCodebergLastRel($app) {
    $host = parse_url($app['url'])['host'];
    $path = explode('/',parse_url($app['url'])['path']);
    $owner = $path[1]; $repo = $path[2];
    $context = $this->codebergStreamContext();

    $this->log->debug($app['id'].": looking for 'https://$host/api/v1/repos/${owner}/${repo}/releases'");
    $tags = json_decode( file_get_contents("https://$host/api/v1/repos/${owner}/${repo}/releases",false,$context) );

    $lastRelNo = 0;
    $file = '';
    $name = '';
    $aum = $this->getAum($app);

    // if tags exist, now walk them until we find the one we need
    if ( is_array($tags) ) foreach ( $tags as $tag ) { // $tags is array[0..n] of tags
      if ( $app['skipPre'] && $tag->prerelease ) continue;
      if ( !empty($aum) && !preg_match("!${aum}!",$tag->tag_name,$tagmatch) ) continue;
      if ( empty($aum) ) {
        $lastRelNo = $tag->tag_name;
      } else {
        $lastRelNo = @preg_replace("!${aum}!",'$1',$tag->tag_name);
      }
      $lastRelNo = @preg_replace('![^\d\.]!','',$lastRelNo);
      if ( strpos($lastRelNo,'.')===0 ) $lastRelNo = substr($lastRelNo,1); // v.1.2.3 => .1.2.3 => 1.2.3

      $files = [];
      foreach ( $tag->assets as $ass ) $files[] = ['name'=>$ass->name,'url'=>$ass->browser_download_url,'timestamp'=>strtotime($ass->created_at)];
      if ( count($files) > 1 ) { // multiple APKs; most likely -debug, -test etc. amongst them
        foreach ($files as $f) {
          if ( $f['timestamp'] <= $app['filetimestamp'] ) continue;                                         // not newer than what we already have
          if ( !preg_match('!\.apk$!',$f['name']) ) continue;                                               // not an APK file at all
          if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$f['name']) ) continue;             // does not match as defined by MetaData
          if ( !empty($app['apkignore']) && preg_match($app['apkignore'],$f['name']) ) continue;            // MetaData told us to ignore this
          if ( !empty($app['apkmatch']) && preg_match($app['apkmatch'],$f['name']) ) { $file = $f['url']; $name = $f['name']; break; } // match as defined by MetaData
          if ( empty($app['apkmatch']) && preg_match('!release.apk$!',$f['name']) ) { $file = $f['url']; break; }   // this is obviously what we want otherwise
          if ( preg_match('!-(debug|test).apk$!',$f['name']) ) continue;                                    // no dev versions, please :)
          $file = $f['url'];                                                                                // we take what's left
          $name = $f['name'];
        }
        if ( empty($file) ) {
          $this->log->debug("No true match found for ".$app['id']);
        }
      } elseif (!empty($files)) {
        if ( $files[0]['timestamp'] <= $app['filetimestamp'] ) $file = '';                                  // not newer than what we already have
        if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$files[0]['name']) ) $file = '';      // does not match as defined by MetaData
        elseif ( !empty($app['apkignore']) && preg_match($app['apkignore'],$files[0]['name']) ) $file = ''; // MetaData told us to ignore this
        elseif ( !preg_match('!\.apk$!',$files[0]['name']) ) $file = '';                                    // not an APK file at all
        else { $file = $files[0]['url']; $name = $files[0]['name']; }
      }

      break; // this was the latest matching tag
    }
    $this->log->debug($app['id'].": returning ['$lastRelNo','" . trim($file) ."','$name']");
    return ['rel'=>$lastRelNo,'file'=>trim($file),'name'=>$name];
  }


// ----------------------------------------------------------------------------
  /** List directory structure from Gitea/Codeberg
   * @method getCodebergRepoPath
   * @param array spec      see this::getRemoteRepoMeta
   * @param optional string notFoundLevel   logLevel for failed requests, default 'warn'
   * @return array meta     see this::getRemoteRepoMeta
   * @brief used for listing Fastlane details; uses GiteaAPI https://try.gitea.io/api/swagger#/repository/repoGetContents
   */
  function getCodebergRepoPath($spec,$notFoundLevel='warn') {
    if ( !in_array($notFoundLevel,['emergency','critical','error','warn','info','debug']) ) $notFoundLevel = 'warn';
    $host = $spec['host']; $owner = $spec['owner']; $repo = $spec['repo']; $path = $spec['path']; $appId = $spec['appId'];
    $files = [];
    $context = $this->codebergStreamContext();
    $this->log->debug("$appId: looking for 'https://${host}/api/v1/repos/${owner}/${repo}/contents/${path}'");
    $tag = json_decode( file_get_contents("https://${host}/api/v1/repos/${owner}/${repo}/contents/${path}",false,$context) );
    if ( empty($tag) || is_array($tag) && is_string($tag[0]) && preg_match('!object does not exist!',$tag[0]) ) { // not found = 404 with empty response
      $this->log->{$notFoundLevel}("${appId}: HTTP request failed for 'https://${host}/api/v1/repos/${owner}/${repo}/contents/${path}'");
      return $files;
    }
    if ( is_object($tag) && property_exists($tag,'errors') ) {
      $this->log->{$notFoundLevel}("${appId}: " . $tag->errors[0]); // {"errors":["object does not exist [id: , rel_path: .github]"],"message":"GetContentsOrList","url":"https:\/\/codeberg.org\/api\/swagger"}
      return $files;
    }
    foreach($tag as $file) {
      if ( ! (is_object($file) && property_exists($file,'type')) ) {
        if ( is_object($tag) && property_exists($tag,'type') && property_exists($tag,'name') && property_exists($tag,'path') && property_exists($tag,'download_url') ) $file = $tag; // single file like Readme, e.g. with https://codeberg.org/y20k/transistor/src/commit/849706746a1d5f6ce4a1f08ff60c6371294362bd/metadata
        else {
          $this->log->warn("${appId}: file '$path' has no type, skipping (".json_encode($tag).")");
          continue;
        }
      }
      switch($file->type) { // can be dir, file, symlink, submodule
        case 'dir':
          $files[] = ['name'=>$file->name,'path'=>$file->path,'type'=>'tree','url'=>null,'ts'=>null];
          break;
        case 'file':
          if ( property_exists($file,'last_commit_sha') ) $commit = json_decode( file_get_contents("https://${host}/api/v1/repos/${owner}/${repo}/commits?limit=1&sha=".$file->last_commit_sha,false,$context) );
          else $commit = ''; // missing before Gitea 1.17
          if ( is_array($commit) ) {
            $files[] = ['name'=>$file->name,'path'=>$file->path,'type'=>'file','url'=>$file->download_url,'ts'=>date_timestamp_get(date_create($commit[0]->commit->committer->date))];
          } else { // API is incomplete until 1.17 (SHA is not commit, but file hash; last_commit_sha not introduced before Gitea 1.17)
            $this->log->notice("${appId}: no commit found, probably hit Gitea Bug #12840 - cannot evaluate timestamps! Assuming now().");
            $files[] = ['name'=>$file->name,'path'=>$file->path,'type'=>'file','url'=>$file->download_url,'ts'=>time()];
          }
          break;
        case "submodule": //  {str name, str path, str sha, str last_commit_sha, str type, int size, ? encoding, ? content, ? target (null), str url, str html_url git_url download_url, str submodule_git_url, json_str _links}
          break;
        default:
          $this->log->{$notFoundLevel}("${appId}: unsupported type '".$file->type."' for '".$file->path."'");
          break;
      }
    }
    return $files;
  }


###################################################################[ Github ]###
  /** Get the URL context needed for Github API queries (stream_context_create)
   * @method githubStreamContext
   * @protected
   * @return context created via githubStreamContext
   * @brief used by getGithubLastRel, getGithubRepoApks, getGithubRepoPath
   */
  protected function githubStreamContext() {
    $opts = array('http' =>
      array(
        'method' => 'GET',
        'header' => "Accept-language: en\r\nAccept-Charset: UTF-8\r\nAuthorization: Basic ".base64_encode($this->conf['github']['user'].':'.$this->conf['github']['pass'])."\r\n",
        'user_agent' => $this->ua,
        'ignore_errors' => TRUE,
        'timeout' => 90 // default_socket_timeout = 60
      )
    );
    return stream_context_create($opts);
  }

// ----------------------------------------------------------------------------
  /** Get details on a specified Github repository
   * @method getGithubRepoInfo
   * @param array spec      [str host, str owner, str repo, str appId]
   * @return object info    empty when server returned neither 200 nor 404, else repo details (see this::repoInfo)
   */
  function getGithubRepoInfo($spec) {
    $context = $this->githubStreamContext();
    foreach($spec as $key => $val) ${$key} = $val;

    $this->log->debug($appId.": looking for 'https://api.github.com/repos/${owner}/${repo}'");

    $info = new stdClass();
    $repo = json_decode( file_get_contents("https://api.github.com/repos/${owner}/${repo}",false,$context) );
    $resp = explode(' ',$http_response_header[0])[1];
    if ( $resp == "200" || $resp = "301" ) { // found or moved
      $this->log->debug($appId.": got repo details, parsing now.");
      if ( !property_exists($repo,'created_at') ) {
        $this->log->warn($appId . ": Guthub responded with '$resp' but returned an empty record. API limits hit?");
        if ( property_exists($repo,'message') ) {
          $this->log->warn($appId . ": message is: '" . $repo->message . "'");
          if ( strpos($repo->message,'rate limit exceeded') ) {
            $tmp = @file_get_contents('https://api.github.com/users/' . $this->conf['github']['user']); $tmp = '';
            foreach ($http_response_header as $header) if ( in_array(explode(':',$header), ['X-RateLimit-Limit','X-RateLimit-Remaining','X-RateLimit-Reset']) ) $tmp .= " ${header}";
            $this->log->warn($appId . ": Limits:${tmp}");
          }
        }
        if ( property_exists($repo,'documentation_url') ) $this->log->warn($appId . ": URL given for details: '" . $repo->documentation_url . "'");
        return $info;
      }
      foreach(['created_at', 'updated_at', 'default_branch', 'description', 'has_issues', 'has_wiki', 'name', 'size', 'ssh_url', 'html_url', 'archived'] as $val) $info->{$val} = $repo->{$val};
      $info->website = $repo->homepage;
      ( property_exists($repo,'license') && is_object($repo->license) && property_exists($repo->license,'spdx_id') ) ? $info->license = $repo->license->spdx_id : $info->license = null;
      if ( $info->license == 'NOASSERTION' ) $info->license = null; // Github could not figure from the LICENSE file
      $info->gone = 0;
    } elseif ( $resp == "403" ) {
      $this->log->warn($appId.": getGithubRepoInfo got a 403, secondary API limits hit!");
    } elseif ( $resp == "404" ) {
      $this->log->debug($appId.": got a 404, repo gone.");
      $info->gone = 1;
    } else {
      $this->log->debug($appId.": got a $resp response, giving up.");
    }

    return $info;
  }


// ----------------------------------------------------------------------------
  /** Check Github Releases page of an app for latest version
   * @method getGithubLastRel
   * @param array app   app data: id (packageName), url (repo URL), method ('github-release', used for logging), aum (AutoUpdateMode), skipPre (0/1: skip pre-releases?), apkmatch/apkignore (RegEx on filename), ver (current version), filetimestamp (timestamp of latest local APK for this app), aum (AutoUpdateMode)
   * @return array[str rel, str file] releaseNo and file name of latest release found (local or upstream)
   * @brief Using GithubAPI, see: https://developer.github.com/v3/repos/releases/
   */
  function sortGHRelByPubDate($a, $b) { // reverse sort by pubdate
    if ($a->published_at < $b->published_at) return 1;
    elseif ($a->published_at > $b->published_at) return -1;
    else return 0;
  }
  function getGithubLastRel($app) {
    $path = explode('/',parse_url($app['url'])['path']);
    $owner = $path[1]; $repo = $path[2];
    $context = $this->githubStreamContext();

    $this->log->debug($app['id'].": looking for 'https://api.github.com/repos/${owner}/${repo}/releases'");
    $tags = @file_get_contents("https://api.github.com/repos/${owner}/${repo}/releases",false,$context);

    // do we have tags?
    if ( empty($tags) ) {
      $this->log->error($app['id'].' is set for "'.$app['method'].'" but has not a single release (or connection timed out)');
      return ['rel'=>0,'file'=>''];
    } else {
      $tags = json_decode( $tags );
      if ( $tags == null ) $this->log->error("getGithubLastRel: no releases found; tags is null for ".$app['id']);
      else @usort($tags,'upstream::sortGHRelByPubDate'); // GH messed up here, see https://github.com/laurent22/joplin/issues/15#issuecomment-1511710544
    }
    if ( is_object($tags) && isset($tags->message) ) {
      $this->log->error('Github reports for '.$app['id'].': '.$tags->message);
      return ['rel'=>0,'file'=>''];
    }

    $aum = $this->getAum($app);

    // tags exist, now walk them until we find the one we need
    $lastRelNo = 0;
    $file = '';
    foreach ( $tags as $tag ) { // $tags is array[0..n] of tags
      $this->log->debug($app['id'].": checking tag '".$tag->tag_name."'");
      if ( $app['skipPre'] && $tag->prerelease ) continue;
      if ( !empty($aum) && !preg_match("!${aum}!",$tag->tag_name,$tagmatch) ) continue;
      if ( empty($aum) ) {
        $lastRelNo = $tag->tag_name;
      } else {
        $lastRelNo = @preg_replace("!${aum}!",'$1',$tag->tag_name);
      }
//      $lastRelNo = @preg_replace('![^\d\.]!','',$lastRelNo);
      if ( strpos($lastRelNo,'.')===0 ) $lastRelNo = substr($lastRelNo,1); // v.1.2.3 => .1.2.3 => 1.2.3
      $this->log->debug($app['id'].": lastRelNo set to '$lastRelNo', checking for files");

      $files = []; $fdates = [];
      foreach ( $tag->assets as $ass ) {
        $files[] = $ass->browser_download_url;
        $fdates[] = $ass->updated_at;
      }
      if ( count($files) > 1 ) { // multiple APKs; most likely -debug, -test etc. amongst them
        foreach ($files as $num => $f) {
          if ( !preg_match('!\.apk$!',$f) ) continue;                                               // not an APK file at all
          if ( !empty($app['apkignore']) && preg_match($app['apkignore'],$f) ) continue;            // MetaData told us to ignore this
          if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$f) ) continue;             // does not match as defined by MetaData
          if ( !empty($app['apkmatch']) && preg_match($app['apkmatch'],$f) ) {                      // match as defined by MetaData
            $file = $f;
            $fdate = strtotime($fdates[$num]);
            break;
          }
          if ( empty($app['apkmatch']) && preg_match('!release.apk$!',$f) ) {                       // this is obviously what we want
            $file = $f; $fdate = strtotime($fdates[$num]); break;
          }
          if ( preg_match('!-(debug|test).apk$!',$f) ) continue;                                    // no dev versions, please :)
          $file = $f;                                                                               // we take what's left
          $fdate = strtotime($fdates[$num]);
        }
        if ( empty($file) ) {
          $this->log->debug("No true match found for ".$app['id']);
        }
      } elseif (!empty($files)) {
        if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$files[0]) ) $file = '';      // does not match as defined by MetaData
        elseif ( !empty($app['apkignore']) && preg_match($app['apkignore'],$files[0]) ) $file = ''; // MetaData told us to ignore this
        else {
          $file = $files[0];
          $fdate = strtotime($fdates[0]);
        }
      }
      if ( empty($file) ) $fdate = $app['filetimestamp'];
      if ( $fdate <= $app['filetimestamp'] ) { // avoid fetching files we already have, e.g. because versionName does not match tag name (see #10)
        $this->log->debug("Upstream file date (".date('Y-m-d H:i',$fdate).") not newer than ours (".date('Y-m-d H:i',$app['filetimestamp'])."), skipping.");
        if ( strpos($app['aum'],'%c')!==false ) {
          $this->log->debug($app['id'].": returning local appdata as [".$app['vercode'].",'']");
          return ['rel'=>$app['vercode'],'file'=>''];
        } else {
          $this->log->debug($app['id'].": returning local appdata as ['".$app['ver']."','']");
          return ['rel'=>$app['ver'],'file'=>''];
        }
      } else {
        $this->log->debug("Upstream file date (".date('Y-m-d H:i',$fdate).") is newer than ours (".date('Y-m-d H:i',$app['filetimestamp']).").");
      }

      break; // this was the latest matching tag
    }

    $this->log->debug($app['id'].": returning ['$lastRelNo','" . trim($file) ."']");
    return ['rel'=>$lastRelNo,'file'=>trim($file)];
  }

// ----------------------------------------------------------------------------
  /** Get all *.apk file names from a given GH repo page together with their timestamps (date)
   * @method getGithubRepoApks
   * @param array app   app data: id (packageName), url (repo URL), method ('github-repo', used for logging), aum (AutoUpdateMode), skipPre (0/1: skip pre-releases?), apkmatch/apkignore (RegEx on filename), ver (current version), filetimestamp (timestamp of latest local APK for this app)
   * @return array[date=>file]; date: YYYY-MM-DD
   * @brief using Github API, see https://developer.github.com/v3/repos/contents/ and https://developer.github.com/v3/repos/commits/
   */
  function getGithubRepoApks($app) {
    $context = $this->githubStreamContext();
    $path = explode('/',parse_url($app['url'])['path']);
    $owner = $path[1]; $repo = $path[2];
    if ( count($path) > 3 && in_array($path[3],['blob','raw','tree']) && in_array($path[4],['master','main'])) $k = 5;
    else $k = 3;
    if ( isset($path[$k]) ) {
      $dir = $path[$k]; if ($k < count($path)-1) for ($i=$k+1;$i<count($path);++$i) $dir .= '/'.$path[$i];
    } else $dir = '';
    $this->log->debug($app['id'].": looking for 'https://api.github.com/repos/${owner}/${repo}/contents/${dir}'");
    $tag = json_decode( file_get_contents("https://api.github.com/repos/${owner}/${repo}/contents/${dir}",false,$context) );
    $files = [];
    if ( (is_array($tag) && isset($tag['message'])) || (is_object($tag) && property_exists($tag,'message')) ) { // an error occured
      if ( is_array($tag) ) $this->log->error($app['id'].': Github reports "'.$tag['message']."\" for https://api.github.com/repos/${owner}/${repo}/contents/$dir");
      else  $this->log->error($app['id'].': Github reports "'.$tag->message."\" for https://api.github.com/repos/${owner}/${repo}/contents/$dir");
      return [];
    }
    foreach($tag as $file) {
      if ( $file->type == 'file' && preg_match('!\.apk$!',$file->name) ) {
        if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$file->name) ) continue;         // does not match as defined by MetaData
        elseif ( !empty($app['apkignore']) && preg_match($app['apkignore'],$file->name) ) continue;    // MetaData told us to ignore this
        $dir = str_replace(' ','%20',$file->path); // file->path might contain spaces, e.g. for com.colorcloud.wifichat
        $commits = json_decode( file_get_contents("https://api.github.com/repos/${owner}/${repo}/commits?path=$dir",false,$context) );
        if ( is_array($commits) ) $date = strtotime($commits[0]->commit->committer->date);
        else $date = strtotime($commits->commit->committer->date); // just a single commit?
        $files[$date] = $file->download_url;
      }
    }
    krsort($files);
    return $files;
  }


// ----------------------------------------------------------------------------
  /** List directory structure from Github
   * @method getGithubRepoPath
   * @param array spec      see this::getRemoteRepoMeta
   * @return array meta     see this::getRemoteRepoMeta
   * @brief used for listing Fastlane details; using Github API, see https://developer.github.com/v3/repos/contents/ and https://developer.github.com/v3/repos/commits/
   */
  function getGithubRepoPath($spec,$notFoundLevel='warn') {
    if ( !in_array($notFoundLevel,['emergency','critical','error','warn','info','debug']) ) $notFoundLevel = 'warn';
    $host = $spec['host']; $owner = $spec['owner']; $repo = $spec['repo']; $dir = str_replace('/','%2F',$spec['path']); $appId = $spec['appId'];
    $files = [];
    $context = $this->githubStreamContext();
    $this->log->debug("$appId: looking for 'https://api.github.com/repos/${owner}/${repo}/contents/${dir}'");
    $tag = @file_get_contents("https://api.github.com/repos/${owner}/${repo}/contents/${dir}",false,$context);

    if (empty($tag)) {
      $this->log->error("Could not retrieve repo directory structure for $appId (connection issues?)");
      return [];
    } else {
      $tag = json_decode( $tag );
    }

    if ( (is_array($tag) && isset($tag['message'])) || (is_object($tag) && property_exists($tag,'message')) ) { // an error occured
      if ( is_array($tag) ) $this->log->{$notFoundLevel}($appId.': Github reports "'.$tag['message']."\" for https://api.github.com/repos/${owner}/${repo}/contents/$dir");
      else  $this->log->{$notFoundLevel}($appId.': Github reports "'.$tag->message."\" for https://api.github.com/repos/${owner}/${repo}/contents/$dir");
      return [];
    }
    if ( !is_array($tag) ) $tag = [$tag]; // single file
    foreach($tag as $file) {
      if ( empty($file) ) {
        $this->log->warn($appId.": skipping empty entry for getGithubRepoPath(".$spec['path'].")");
        continue;
      }
      switch($file->type) { // dir, file, symlink ($file->target then has target path), submodule ($file->submodule_git_url then has the git:// url)
        case 'file':
          $dir = str_replace(' ','%20',$file->path); // file->path might contain spaces, e.g. for com.colorcloud.wifichat
          $commits = json_decode( file_get_contents("https://api.github.com/repos/${owner}/${repo}/commits?path=$dir",false,$context) );
          if ( is_array($commits) ) $date = strtotime($commits[0]->commit->committer->date);
          elseif ( is_object($commits) ) { // just a single commit?
            if ( property_exists($commits,'commit') ) $date = strtotime($commits->commit->committer->date);
            else { $date = ''; $this->log->error($appId.": Cannot retrieve commit details for '".$file->path."' on spec '".$spec['path']."', commit date remains empty."); }
          }
          else { $date = ''; $this->log->error($appId.": Commits neither array nor object for '".$file->path."' on spec '".$spec['path']."' - cannot set ts/date"); }
          $files[] = ['name'=>$file->name,'path'=>$file->path,'type'=>'file','url'=>$file->download_url,'ts'=>$date];
          break;
        case 'dir':
          $files[] = ['name'=>$file->name,'path'=>$file->path,'type'=>'tree','url'=>null,'ts'=>null];
          break;
        default:
          $this->log->{$notFoundLevel}($appId.": unsupported type '".$file->type."' for '".$file->path."'");
          break;
      }
    }
    return $files;
  }


###################################################################[ GitLab ]###
  /** Get details on a specified GitLab repository
   * @method getGitlabRepoInfo
   * @param array spec      [str host, str owner, str repo, str appId]
   * @return object info    empty when server returned neither 200 nor 404, else repo details (see this::repoInfo)
   */
  function getGitlabRepoInfo($spec) {
    foreach($spec as $key => $val) ${$key} = $val;
    $this->log->debug($appId.": looking for 'https://${host}/api/v4/projects/${owner}%2F${repo}'");
    $commit_url = "https://${host}/api/v4/projects/${owner}%2F${repo}/repository/commits";
    if ( empty($this->conf['gitlab']) || empty($this->conf['gitlab']['token']) || empty($this->conf['gitlab']['token'][$host]) ) {
      $this->log->info("for full details on '${host}:${owner}/${repo}', getGitlabRepoInfo() requires a token to be set up in your .ini for '$host'");
      $repo = json_decode( @file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}?license=true") );
    } else {
      $repo = json_decode( @file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}?license=true&access_token=".$this->conf['gitlab']['token'][$host]) );
    }

    $info = new stdClass();
    $resp = explode(' ',$http_response_header[0])[1];
    if ( $resp == "200" ) {
      $this->log->debug($appId.": got repo details, parsing now.");
      foreach(['created_at', 'default_branch', 'description', 'name'] as $val) $info->{$val} = $repo->{$val};
      $info->ssh_url = $repo->ssh_url_to_repo;
      $info->html_url = $repo->web_url; // http_url_to_repo ends in .git
      $info->size = null;
      $info->website = null; // $repo->web_url points to the repo itself (same as html_url_to_repo just without the .git suffix), no field for website here
      if ( empty($this->conf['gitlab']) || empty($this->conf['gitlab']['token']) || empty($this->conf['gitlab']['token'][$host]) ) { // elements only accessible with OAUTH token
        $info->has_issues = $info->has_wiki = $info->license = $info->archived = null;
      } else {
        $info->has_issues = $repo->issues_enabled;
        $info->has_wiki = $repo->wiki_enabled;
        // $info->size = $repo->statistics->repository_size; // project members reporter+ only – WTF?
        if ( property_exists($repo,'license') && is_object($repo->license) && property_exists($repo->license,'key') ) $info->license = $repo->license->key;
        else $info->license = null;
        $info->archived = $repo->archived;
      }
      $info->gone = 0;

      $commits = json_decode( file_get_contents($commit_url) );
      $info->updated_at = $commits[0]->committed_date;
    } elseif ( $resp == "404" ) {
      $this->log->debug($appId.": got a 404, repo gone.");
      $info->gone = 1;
    } else {
      $this->log->debug($appId.": got a $resp response, giving up.");
    }

    return $info;
  }


// ----------------------------------------------------------------------------
  /** Get all *.apk file names from a given GL repo page together with their timestamps (date)
   * @method getGitlabRepoApks
   * @param array app   app data: id (packageName), url (repo URL), method ('gitlab-repo', used for logging), aum (AutoUpdateMode), skipPre (0/1: skip pre-releases?), apkmatch/apkignore (RegEx on filename), ver (current version), filetimestamp (timestamp of latest local APK for this app)
   * @return array[date=>file]; date: YYYY-MM-DD
   * @brief APKs inside GitLab repos, using GitLab API https://docs.gitlab.com/ee/api/repositories.html
   */
  function getGitlabRepoApks($app) {
    $apath = parse_url($app['url']);
    $path = explode('/',$apath['path']);
    $owner = $path[1]; $repo = $path[2];
    $repodef = json_decode(@file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}"));
    if ( ! ( is_object($repodef) && property_exists($repodef,'default_branch') && $branch = $repodef->default_branch ) ) $branch = 'master'; // master or main? Needed for URLs
    if ( count($path) > 3 ) {
      unset($path[2],$path[1],$path[0]);
      if ( $path[3] == '-' ) unset($path[3]);   // when copied from browser, '-/blob/<branch>/' is inserted which the API does not understand
      if ( in_array($path[4],['blob','tree']) ) unset($path[4],$path[5]);
      $fpath = implode('%2F',$path);
    } else $fpath = '';
    $files = [];
    $this->log->debug($app['id'].": looking for 'https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}'");
    if ( !$tag = @file_get_contents("https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}") ) {
      $this->log->error($app['id'].": HTTP Request to GitLab failed for 'https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}'");
      return $files;
    }
    foreach(json_decode($tag) as $node) {
      if ( preg_match('!\.apk$!',$node->path) ) {
        if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$node->name) ) continue;             // does not match as defined by MetaData
        if ( !empty($app['apkignore']) && preg_match($app['apkignore'],$node->name) ) continue;            // MetaData told us to ignore this
        $commit = file_get_contents("https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/commits?path=".str_replace('/','%2F',$node->path));
        if ( !empty($app['apkmatch']) && preg_match($app['apkmatch'],$node->name) ) {                      // match as defined by MetaData
          return [strtotime(json_decode($commit)[0]->created_at) => "https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/files/".str_replace('/','%2F',$node->path)."/raw?ref=${branch}"];
        }
        $files[strtotime(json_decode($commit)[0]->created_at)] = "https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/repository/files/".str_replace('/','%2F',$node->path)."/raw?ref=${branch}";
      }
    }
    return $files;
  }

// ----------------------------------------------------------------------------
  /** Check GitLab Releases page of an app for latest version
   * @method getGitLabLastRel
   * @param array app   app data: id (packageName), url (repo URL), method ('gitlab-tags', used for logging), aum (AutoUpdateMode), skipPre (0/1: skip pre-releases?), apkmatch/apkignore (RegEx on filename), ver (current version), filetimestamp (timestamp of latest local APK for this app)
   * @return array[str rel, str file] releaseNo and file name of latest release
   * @brief GitLab releases using GitLab releases API https://docs.gitlab.com/ee/api/releases/
   * @verbatim
   *  * example for assets: https://gitlab.shinice.net/api/v4/projects/pixeldroid%2FPixelDroid/releases
   *  * example for description: https://gitlab.com/api/v4/projects/fasheng%2Famap-nlp-backend/releases
   */
  function getGitLabLastRel($app) {
    $apath = parse_url($app['url']);
    $path = explode('/',$apath['path']);
    $owner = $path[1]; $repo = $path[2];
    $this->log->debug($app['id'].": looking for 'https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/releases'");
    $tag = @file_get_contents("https://".$apath['host']."/api/v4/projects/${owner}%2F${repo}/releases");

    if (empty($tag)) {
      $this->log->error('Could not retrieve tags for '.$app['id']);
      return ['rel'=>0,'file'=>''];
    }

    $tag = json_decode($tag);
    $aum = $this->getAum($app);

    $lastRelNo = '';
    foreach ($tag as $tid => $titem) {
      $this->log->debug($app['id'].": checking tag '".$titem->tag_name."'");
      if ( !empty($aum) && !preg_match("!${aum}!",$titem->tag_name,$tagmatch) ) continue;
      if ( empty($aum) ) {
        $lastRelNo = $titem->tag_name;
      } else {
        $lastRelNo = @preg_replace("!${aum}!",'$1',$titem->tag_name);
      }
      $this->log->debug($app['id'].": lastRelNo set to '".$titem->tag_name."'");
      break;
    }

    $this->log->debug($app['id'].": investigating tag '".$titem->tag_name."' for files");
    if ( empty($lastRelNo) || date_timestamp_get(date_create($tag[$tid]->commit->created_at)) <= $app['filetimestamp'] ) return ['rel'=>$app['ver'],'file'=>'']; // tag is not newer than our APK

    @preg_match_all('![="\'](http[^"\'>]+?apk)!', $tag[$tid]->description, $files);
    @preg_match_all('!\((\S+apk)\)!', $tag[$tid]->description, $filesa); $files2 = []; // GitLab now uses Markdown: '[name](/rel_url)'
    foreach($filesa[1] as $file) if (!preg_match('!^https!',$file)) $files2[] = 'https://'.$apath['host'].'/'.$owner.'/'.$repo.$file;
    $files3 = [];
    if ( isset($tag[$tid]->assets->links) ) foreach ($tag[$tid]->assets->links as $link) {
      $files3[] = preg_replace('!(/jobs/\d+/artifacts)/file/!','$1/raw/',$link->url);
    }
    $files = array_merge($files[1],$files2,$files3);
    if ( empty($files) ) {
      $this->log->error("No files found at tag ${lastRelNo} for ".$app['id']);
      return ['rel'=>0,'file'=>''];
    }
    $prerelease = false;

    $lastRelNo = @preg_replace('![^\d\.]!','',$lastRelNo);
    if ( strpos($lastRelNo,'.')===0 ) $lastRelNo = substr($lastRelNo,1); // v.1.2.3 => .1.2.3 => 1.2.3
    $this->log->debug($app['id'].": lastRelNo adjusted to '$lastRelNo'");

    if ( count($files) > 1 ) { // multiple APKs; most likely -debug, -test etc. amongst them
      $file = '';
      foreach ($files as $f) {
        if ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$f) ) continue;             // does not match as defined by MetaData
        if ( !empty($app['apkignore']) && preg_match($app['apkignore'],$f) ) continue;            // MetaData told us to ignore this
        if ( !empty($app['apkmatch']) && preg_match($app['apkmatch'],$f) ) { $file = $f; break; } // match as defined by MetaData
        if ( empty($app['apkmatch']) && preg_match('!release.apk$!',$f) ) { $file = $f; break; }  // this is obviously what we want
        if ( preg_match('!-(debug|test).apk$!',$f) ) continue;                                    // no dev versions, please :)
        $file = $f;                                                                               // we take what's left
      }
      if ( empty($file) ) {
        $this->log->debug("No true match found for ".$app['id']);
      }
    } elseif (empty($files)) {
      $file = '';
    } else {
      if ( !empty($app['apkmatch']) && preg_match($app['apkmatch'],$files[0]) ) $file = $files[0]; // match as defined by MetaData
      elseif ( !empty($app['apkmatch']) && !preg_match($app['apkmatch'],$files[0]) ) $file = '';  // does not match as defined by MetaData
      elseif ( !empty($app['apkignore']) && preg_match($app['apkignore'],$files[0]) ) $file = ''; // MetaData told us to ignore this
      elseif ( preg_match('!release.apk$!',$files[0]) ) $file = $files[0];                        // this is obviously what we want
      else $file = $files[0];
    }

    if ($prerelease && $app['skipPre']==1) { $rel = 0; $file = ''; }
    else { $rel = $lastRelNo; $file = trim($file); }
    $this->log->debug($app['id'].": returning ['$rel','$file']");
    return ['rel'=>$rel,'file'=>$file];
  }

// ----------------------------------------------------------------------------
  /** List directory structure from GitLab
   * @method getGitlabRepoPath
   * @param array spec      see this::getRemoteRepoMeta
   * @param optional string notFoundLevel   logLevel for failed requests, default 'warn'
   * @return array meta     see this::getRemoteRepoMeta
   * @see https://docs.gitlab.com/ee/api/repository_files.html / https://docs.gitlab.com/ee/api/repositories.html
   * @brief used for listing Fastlane details
   */
  function getGitlabRepoPath($spec,$notFoundLevel='warn') {
    if ( !in_array($notFoundLevel,['emergency','critical','error','warn','info','debug']) ) $notFoundLevel = 'warn';
    $host = $spec['host']; $owner = $spec['owner']; $repo = $spec['repo']; $fpath = str_replace('/','%2F',$spec['path']); $appId = $spec['appId'];
    $files = [];
    $repodef = json_decode(@file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}"));
    if ( ! ( is_object($repodef) && property_exists($repodef,'default_branch') && $branch = $repodef->default_branch ) ) $branch = 'master'; // master or main? Needed for URLs
    $this->log->debug("$appId: looking for 'https://${host}/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}'");
    if ( !$tag = @file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}") ) {
      $this->log->{$notFoundLevel}($appId.": HTTP Request to GitLab failed for 'https://${host}/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}'");
      return $files;
    }
    if ( strpos($tag,'<!DOCTYPE html>')===0 ) {
      $this->log->{$notFoundLevel}($appId.": HTTP Request to GitLab returned HTML instead of JSON 'https://${host}/api/v4/projects/${owner}%2F${repo}/repository/tree?path=${fpath}' (probably requires sign-in)");
      return $files;
    }
    foreach(json_decode($tag) as $node) {
      switch($node->type) { // tree, blob; no symlinks/submodules here
        case 'tree': $files[] = ['name'=>$node->name,'path'=>$node->path,'type'=>'tree','url'=>null,'ts'=>null]; break;
        case 'blob':
          if ( $commit = file_get_contents("https://${host}/api/v4/projects/${owner}%2F${repo}/repository/commits?path=".str_replace('/','%2F',urlencode($node->path))) ) {
            $files[] = ['name'=>$node->name,'path'=>$node->path,'type'=>'file','url'=>"https://${host}/${owner}/${repo}/-/raw/${branch}/".$node->path,'ts'=>date_timestamp_get(date_create(json_decode($commit)[0]->created_at))];
          } else {
            $this->log->{$notFoundLevel}($appId.": cannot set creation timestamp, failed to retrieve commit for '".$node->path."' on spec '".$spec['path']."'.");
            $files[] = ['name'=>$node->name,'path'=>$node->path,'type'=>'file','url'=>"https://${host}/${owner}/${repo}/-/raw/${branch}/".$node->path,'ts'=>''];
          }
          break;
        default:
          $this->log->{$notFoundLevel}($appId.": unsupported type '".$node->type."' for '".$node->path."'");
          break;
      }
    }
    return $files;
  }


#########################################################[ Global wrappers ]###
  /** List remote directory structure
   *  Currently supports Github, GitLab (gitlab.com as well as self-hosted), Gitea (Codeberg.org or self-hosted)
   * @method getRemoteRepoMeta
   * @param string service  service to obtain data from (gitlab,github,codeberg)
   * @param array spec      [str host, str owner, str repo, str path (no leading/trailing slashes!), str appId]
   * @param optional string notFoundLevel   logLevel for failed requests, default 'warn'
   * @return array meta     [0..n][str name, str path, str type (tree, file), str url (NULL for dirs and when unknown, eg GitLab), int ts (unix timestamp; NULL for dirs)]
   */
  function getRemoteRepoMeta($service,$spec,$notFoundLevel='warn') {
    switch ($service) {
      case 'gitlab': return $this->getGitlabRepoPath($spec,$notFoundLevel); break;
      case 'github': return $this->getGithubRepoPath($spec,$notFoundLevel); break;
      case 'codeberg':
        $spec['path'] = preg_replace('!^[A-z]+/(.+)!','$1',$spec['path']); // strip branch, no support for that
        return $this->getCodebergRepoPath($spec,$notFoundLevel); break;
      default:
        $this->log->warn($app['id'].": unknown service '$service' for Fastlane");
        return [];
        break;
    }
  }

// ----------------------------------------------------------------------------
  /** Get details on an app's repository
   * @method getRepoInfo
   * @param array app      app details
   * @return object info   empty when server returned neither 200 nor 404, else repo details (see below)
   * @verbatim
   *  - created_at    : dateTimeString (eg '2020-03-26T13:59:08.728Z')
   *  - updated_at    : dateTimeString (eg '2020-03-26T13:59:08.728Z')
   *  - default_branch: string (e.g. 'main')
   *  - description   : string (short description of the app)
   *  - name          : string (repo name, as part of the URL)
   *  - ssh_url       : string (e.g. 'git@gitlab.com:giwiniswut/android-media-rw-remounter.git')
   *  - html_url      : string (e.g. 'https://gitlab.com/giwiniswut/android-media-rw-remounter.git')
   *  - size          : int (size in kB; with GitLab always empty, i.e. NULL)
   *  - website       : string (e.g. 'https://gitlab.com/giwiniswut/android-media-rw-remounter'
   *  - has_issues    : int (0|1; whether issue tracker is enabled)
   *  - has_wiki      : int (0|1; whether wiki is enabled)
   *  - license       : string (lowercase with GitLab; e.g. 'mit'; usually matches SPDX, e.g. 'AGPL-3.0' without only/or-later)
   *  - archived      : int (1 or empty; whether rhe repo was archived)
   *  - gone          : int (0|1; 0=got 404 at repo URL, 1=got data)
   */
  function getRepoInfo($app) {
    $host = parse_url($app['url'])['host'];
    $path = explode('/',parse_url($app['url'])['path']);
    $owner = $path[1]; $repo = $path[2];
    $spec = ['host'=>$host,'owner'=>$owner,'repo'=>$repo, 'appId'=>$app['id']];

    switch( preg_replace('!^(.+?)-.*!','$1',$app['method']) ) { // determine service
      case 'github':
        $service = 'github';
        break;
      case 'gitlab':
        $service = 'gitlab';
        break;
      case 'codeberg':
        $service = 'codeberg';
        break;
      default:
        if ( preg_match('!^(github|gitlab|codeberg)\.!i', $host, $match) ) $service = $match[1];
        else $service = ''; // (currently) unsupported, should not come here
        break;
    }

    if ( !empty($service) ) switch ($service) { // get the details
      case 'github'  :
        return $this->getGithubRepoInfo($spec);
        break;
      case 'gitlab'  :
        return $this->getGitlabRepoInfo($spec);
        break;
      case 'codeberg':
        return $this->getCodebergRepoInfo($spec);
        break;
      default        : // we should never reach this point
        $this->log->error("${appId}: unknown service '$service' for getRepoInfo, skipping.");
        return new stdClass();
        break;
    }
    $this->log->error("${appId}: could not determine service for getRepoInfo, skipping.");
    return new stdClass();
  }

// ----------------------------------------------------------------------------
  /** Obtain Fastlane info/structure from remote
   * @method getFastlaneMeta
   * @param string service  service to obtain data from (gitlab,github,codeberg)
   * @param array spec      see this::getRemoteRepoMeta; path is set to the fastlane root (fastlane/metadata/android) if empty
   * @return array meta     [<locale>][text,images,phoneScreenshots][name,path,type,ts (as of getRemoteRepoMeta)]
   * @version currently does not support screenshots other than phoneScreenshots
   * @verbatim
   *  Example calls:
   *    getFastlaneMeta('gitlab',['host'=>'gitlab.com','owner'=>'AuroraOSS', 'repo'=>'AppWarden', 'appId'=>'com.aurora.warden']);   // 4s ± .5s
   *    getFastlaneMeta('github',['host'=>'github.com','owner'=>'SecUSo', 'repo'=>'Aktivpause', 'appId'=>'org.secuso.aktivpause']); // 7s ± .5s
   *    getFastlaneMeta('codeberg',['host'=>'codeberg.org', 'owner'=>'Starfish', 'repo'=>'TinyWeatherForecastGermany', 'appId'=>'de.kaffeemitkoffein.tinyweatherforecastgermany']); // < 2.5s
   *  Codeberg & GitLab could be self-hosted and thus have a different host.
   *  ts can be compared against filemtime to see what needs to be updated.
   */
  function getFastlaneMeta($service,$spec) {
    $files = [];
    if ( empty($spec['path']) ) $spec['path'] = 'fastlane/metadata/android';
    elseif ( substr($spec['path'],0,1) == '/' ) $spec['path'] = substr($spec['path'],1);
    // get locales
    $locales = $this->getRemoteRepoMeta($service,$spec);
    foreach ($locales as $loc) {
      $tfiles = $tchangelogs = $timgs = $tphonescreenshots = [];
      foreach ( $this->getRemoteRepoMeta($service,['host'=>$spec['host'],'owner'=>$spec['owner'],'repo'=>$spec['repo'],'path'=>$spec['path'].'/'.$loc['name'],'appId'=>$spec['appId']]) as $tfile ) {
        if ( preg_match('!\.txt$!',$tfile['name']) ) $tfiles[] = $tfile;
        elseif ( $tfile['name'] == 'images' ) {
          foreach ( $this->getRemoteRepoMeta($service,['host'=>$spec['host'],'owner'=>$spec['owner'],'repo'=>$spec['repo'],'path'=>$spec['path'].'/'.$loc['name'].'/images','appId'=>$spec['appId']]) as $timg ) {
            if ( $timg['type'] == 'file' ) $timgs[] = $timg;
            elseif ( $timg['name'] == 'phoneScreenshots' ) { // only pick phoneScreenshots for now
              foreach ( $this->getRemoteRepoMeta($service,['host'=>$spec['host'],'owner'=>$spec['owner'],'repo'=>$spec['repo'],'path'=>$spec['path'].'/'.$loc['name'].'/images/phoneScreenshots','appId'=>$spec['appId']]) as $tphone ) {
                if ( $tphone['type'] == 'file' ) $tphonescreenshots[] = $tphone;
              }
            }
          }
        } elseif ( in_array($tfile['name'],['changelogs']) ) {
          foreach ( $this->getRemoteRepoMeta($service,['host'=>$spec['host'],'owner'=>$spec['owner'],'repo'=>$spec['repo'],'path'=>$spec['path'].'/'.$loc['name'].'/changelogs','appId'=>$spec['appId']]) as $tlog ) {
            if ( preg_match('!\.txt$!',$tlog['name']) ) $tchangelogs[] = $tlog;
          }
        } else continue; // what should this be?
      }
      $files[$loc['name']] = [];
      $files[$loc['name']]['text'] = $tfiles;
      $files[$loc['name']]['changelogs'] = $tchangelogs;
      $files[$loc['name']]['images'] = $timgs;
      $files[$loc['name']]['phoneScreenshots'] = $tphonescreenshots;
    }
    return $files;
  }


// ----------------------------------------------------------------------------
  /** Obtain FUNDING.yml from remote
   * @method getFundingYml
   * @param string service  service to obtain data from (gitlab,github,codeberg)
   * @param array spec      see this::getRemoteRepoMeta; path is set to the fastlane root (fastlane/metadata/android) if empty
   * @return array meta     [name,path,type,ts (as of getRemoteRepoMeta)]
   */
  function getFundingYml($service,$spec) {
    $tfile = [];
    if ( $gh = $this->getRemoteRepoMeta($service,['host'=>$spec['host'],'owner'=>$spec['owner'],'repo'=>$spec['repo'],'path'=>'.github','appId'=>$spec['appId']],'debug') ) foreach ( $gh as $loc ) {
      if ( $loc['name'] == 'FUNDING.yml' ) { $tfile = $loc; break; }
    }
    if ( empty($tfile) && $gh = $this->getRemoteRepoMeta($service,$spec,'debug') ) foreach ( $gh as $loc ) {
      if ( $loc['name'] == 'FUNDING.yml' ) { $tfile = $loc; break; }
    }
    return $tfile;
  }


// ----------------------------------------------------------------------------
  /** Sanitize HTML
   * @method sanitizeHTML
   * @param string html                 HTML code to be sanitized
   * @param optional string localhost   if specified and not null, links to other hosts are attributed target=_blank and rel=noindex,nofollow
   * @param optional boolean nocompress needed e.g. for changelogs: do not compress to a single line
   * @return string sanitized           sanitized HTML code
   * @brief used mainly for Fastlane full_description.txt. Idea rawly based on https://stackoverflow.com/a/3387568/2533433
   */
  function sanitizeHTML($html,$localhost=null,$nocompress=false) {
    $allowedTags = '<a><b><blockquote><br><center><cite><code><dd><dl><dt><del><div><em><h1><h2><h3><h4><h5><h6><i><hr><kbd><li><ol><p><pre><small><span><sub><sup><strong><strike><tt><u><ul>'; // 'img font ins q s samp';
    $allowedAttribs = ['id','href','src','title','alt','type','lang','style'];
    $html = strip_tags($html,$allowedTags);
    $dom = new DOMDocument();
    $html = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/></head><body>${html}</body></html>";
    $hasPRE= false;
    $dom->loadHTML($html,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    foreach($dom->getElementsByTagName('*') as $node) {
      if (in_array($node->nodeName,['h1','h2','h3','h4','h5','h6'])) { // replace headers as they break structure
        $renode = $dom->createElement('p',$node->nodeValue);
        if (in_array($node->nodeName,['h1','h2','h3'])) $renode->setAttribute('style','font-weight:bold;font-size:larger;');
        else $renode->setAttribute('style','font-weight:bold;');
        $node->parentNode->replaceChild($renode,$node);
      }
      for($i = $node->attributes->length -1; $i >= 0; $i--) {
        $attribute = $node->attributes->item($i);
        if (!in_array($attribute->name,$allowedAttribs)) $node->removeAttributeNode($attribute);
        if ($attribute->name=='href' && preg_match('!\s*javascript!i',$attribute->value)) $node->removeAttributeNode($attribute);
      }
      if ( $localhost!==null && property_exists($node,'tagName') && strtolower($node->tagName)=='a' ) { // set target & rel for external URLs
        $href = $node->getAttribute('href');
        $purl = parse_url($href);
        if ( !array_key_exists('scheme',$purl) ) {
          $this->log->warn("sanitizeHTML: no scheme specified for '$href', skip setting 'target' and 'rel'.");
        } elseif ( in_array(strtolower($purl['scheme']),['http','https']) && $purl['host']!=$localhost ) {
          $node->setAttribute('target','_blank');
          $node->setAttribute('rel','noindex nofollow noopener');
        }
      }
      if ( property_exists($node,'tagName') && strtolower($node->tagName)=='pre' ) $hasPRE = true;
    }
    $html = preg_replace('!.*?<body>(.*)</body></html>!ims','$1',$dom->saveXML());
    if ( !$hasPRE && !$nocompress ) { // compress if there's no PRE tag
      $blockElements = 'address|article|aside|blockquote|canvas|dd|div|dl|dt|fieldset|figcaption|figure|footer|form|h1|h2|h3|h4|h5|h6|header|hr|li|main|nav|noscript|ol|p|pre|section|table|tfoot|ul|video|br';
      $html = preg_replace(['![\x00-\x20]+!','!\A !','! \z!'],[' ','',''],$html);
      $html = preg_replace('!\s*(</?(?:'.$blockElements.')\b(?:[^>/"]+|"[^"]*")*/?>)\s*!','$1',$html);
    }
    return $html;
  }
}

?>