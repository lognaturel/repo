#!/usr/bin/php
<?php
// ============================================================================
// Identify unknown libraries
// © Andreas Itzchak Rehberg
// ----------------------------------------------------------------------------
// This script parses the Smali code handed by our modified LibRadar process
// and checks the libraries found there with the ones we have in our library
// definitions. Missing ones are then pointed out in the corresponding
// *.unknown file so we can add their definitions.
// ----------------------------------------------------------------------------
// This program is free software; you can redistribute and/or modify it under
// the terms of the GNU General Public License (see ../LICENSE)
// ============================================================================

// to store our known definitions in:
$defs = [];

// libraries to ignore (a la "starts-with"). These include Android core libraries,
// some "camouflaged" entries we cannot interpret ("././"), by-products of
// libraries defined otherwise (like those GMS libs drawing in other GMS stuff) …
$defsIgnore = [
 '/.','/./.', '/android/./.',
 '/android/arch',
 '/android/support', '/com/android/(dx|dex|multidex|signapk)',
 '/com/android/mms',
 '/(com|org)/.', '/(com|org)/./.', '/com/(github|google)/./.', '/com/google/firebase/.',
 '/com/google/android/gms/(.|actions|ads|appindexing|base|dynamic|dynamite|flags|gass|internal|measurement|security|signin|tasks)',
 '/com/google/thirdparty',
 '/io/./.',
 '/org/apache/./.',
 '/org/jetbrains/annotations',
 '/rx'
];
// similar, but ignore *anywhere* in the path
$pathIgnore = [
  '/./.[/:]'
];

// Read Smali Defs from file and store definitions in $defs
function setupDefs(&$defs) {
  $locals = file('../lib/libsmali.jsonl');
  foreach($locals as $line) $defs[] = json_decode($line);
}


// Identify modules in a Smali result list
function checkLocals($file) {
  $dirlist = file_get_contents($file);
  $res = [];
  foreach($GLOBALS['defs'] as $def) {
    if ( preg_match('!^\./smali([^/])*' . $def->path . '([/:])!ims',$dirlist) )
      $res[] = ['name'=>$def->name, 'pkgname'=>$def->id, 'type'=>$def->type, 'perms'=>[], 'url'=>$def->url];
  }
  return $res;
}


// Find unidentified entries in a Smali result list
function checkUndef($file) {
  // add the apps own package name to the ignore-List:
  $pkgname = preg_replace('!.*/(.+)_.*!i','$1',$file);
  $pkgname = '/' . str_replace('.','/',$pkgname);
  $defsIgnore = $GLOBALS['defsIgnore'];
  $defsIgnore[] = $pkgname;
  // read the list of package references found:
  $dirlist = file_get_contents($file);
  foreach($GLOBALS['defs'] as $def) {
    $dirlist = preg_replace('!^\./smali([^/])*' . $def->path . '([/:]).*?$\n!ims','',$dirlist);
  }
  // filter out packages we already know (or want to ignore for other reasons):
  foreach($defsIgnore as $def) {
    $dirlist = preg_replace('!^\./smali([^/])*' . $def . '([/:]).*?$\n!ims','',$dirlist);
  }
  foreach($GLOBALS['pathIgnore'] as $pi) $dirlist = preg_replace('!.*'.$pi.'.*!','',$dirlist);
  // if we know 'em all, delete input file – otherwise record unknowns:
  $dirlist = trim(preg_replace('/^[\s]*[\r\n]+/m','',$dirlist)); // empty lines?
  if ( empty($dirlist) ) unlink($file);
  else file_put_contents($file.'.unknowns',$dirlist);
}


// ============================================[ MAIN ]===
setupDefs($defs);

$files = glob('/tmp/*apk.dirlist');
foreach ($files as $file) {
  checkUndef($file);
}
?>